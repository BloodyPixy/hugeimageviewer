//
//  ImageListView.swift
//  HugeImageViewer
//
//  Created by Taras Pasichnyk on 24.07.2020.
//  Copyright © 2020 Taras Pasichnyk. All rights reserved.
//

import UIKit

protocol ImageListViewProtocol: class {
    func setImageItems(_ imageItems: [ImageItem])
    func displayImageRemovedError()
}

class ImageListView: UIViewController {
    
    // MARK: - Strings
    
    struct ImageListViewStrings {
        static let titleText = "image list"
        static let emptyCaseTitle = "Image list is empty"
        static let emptyCaseDescription = "'Documents' folder does not contain any images of type *.tiff"
        static let emtyCaseActionButtonTitle = "Retry scan"
        static let imageRemovedAlertTitle = "Oops"
        static let imageRemovedAlertDescription = "Image you are trying to open may no longer exsit in 'Documents' folder"
        static let imageRemovedActionButtonTitle = "Rescan"
    }

    // MARK: - IBOutlets

    @IBOutlet weak var collectionView: UICollectionView!
    
    // MARK: - Constants
    
    private let numberOfItemsInRow: CGFloat = 3.0
    private let interitemSpacing: CGFloat = 0.0
    private var itemSize: CGSize {
        let fullWidth = collectionView.bounds.width
        let singleItemWidth = fullWidth / numberOfItemsInRow
        let singleItemHeight = singleItemWidth
        
        return CGSize(width: singleItemWidth, height: singleItemHeight)
    }
    
    // MARK: - Variables
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    var presenter: ImageListPresenterProtocol!
    
    var imageItems: [ImageItem] = []

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        configureAppearance()
        configureCollectionView()
        
        presenter.loadImages()
    }

    // MARK: - Configuration

    private func configureAppearance() {
        view.backgroundColor = UIColor.appGrey
        navigationController?.navigationBar.barTintColor = UIColor.appGrey
        navigationController?.navigationBar.isTranslucent = false

        title = ImageListViewStrings.titleText
        let attributes: [NSAttributedString.Key: Any] = [
            NSAttributedString.Key.font: UIFont(name: "Starjedi", size: 24.0) as Any,
            NSAttributedString.Key.foregroundColor: UIColor.appYellow
        ]
        navigationController?.navigationBar.titleTextAttributes = attributes
    }
    
    private func configureCollectionView() {
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.backgroundColor = UIColor.appGrey
        
        collectionView.registerCell(ImageListEmptyCell.self)
        collectionView.registerCell(ImageItemCell.self)
    }
}

// MARK: - ImageListViewProtocol

extension ImageListView: ImageListViewProtocol {
    
    func setImageItems(_ imageItems: [ImageItem]) {
        self.imageItems = imageItems
        collectionView.reloadData()
    }
    
    func displayImageRemovedError() {
        let alert = UIAlertController(
            title: ImageListViewStrings.imageRemovedAlertTitle,
            message: ImageListViewStrings.imageRemovedAlertDescription,
            preferredStyle: .alert)
        let action = UIAlertAction(
            title: ImageListViewStrings.imageRemovedActionButtonTitle,
            style: .default) { [weak self] (action) in
                self?.presenter.loadImages()
        }
        alert.addAction(action)
        present(alert, animated: true)
    }
}

// MARK: - UICollectionViewDataSource

extension ImageListView: UICollectionViewDataSource {
    func collectionView(
        _ collectionView: UICollectionView,
        numberOfItemsInSection section: Int) -> Int {
        if imageItems.isEmpty {
            return 1
        }
        return imageItems.count
    }
    
    func collectionView(
        _ collectionView: UICollectionView,
        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if imageItems.isEmpty {
            let cell = collectionView.dequeueCell(ImageListEmptyCell.self, indexPath: indexPath)
            cell.delegate = self
            cell.setTitle(
                ImageListViewStrings.emptyCaseTitle,
                descriptionText: ImageListViewStrings.emptyCaseDescription)
            cell.setActionButtonTitleText(ImageListViewStrings.emtyCaseActionButtonTitle)
            return cell
        }
        
        let cell = collectionView.dequeueCell(ImageItemCell.self, indexPath: indexPath)
        
        let item = imageItems[indexPath.item]
        cell.setImage(item.thumbnailImage)
        
        return cell
    }
}

// MARK: - UICollectionViewDelegate

extension ImageListView: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if imageItems.isEmpty {
            return
        }
        let item = imageItems[indexPath.item]
        presenter.didSelectItem(item)
    }
}

// MARK: - UICollectionViewDelegateFlowLayout

extension ImageListView: UICollectionViewDelegateFlowLayout {
    
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAt indexPath: IndexPath) -> CGSize {
        if imageItems.isEmpty {
            return collectionView.bounds.size
        }
        return itemSize
    }
    
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return interitemSpacing
    }
    
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return interitemSpacing
    }
    
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
}

// MARK: - ImageListEmptyCellDelegate

extension ImageListView: ImageListEmptyCellDelegate {
    func imageListEmptyCellDidTouchActionButton(_ cell: ImageListEmptyCell) {
        presenter.loadImages()
    }
}
