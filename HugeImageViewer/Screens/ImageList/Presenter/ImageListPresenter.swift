//
//  ImageListPresenter.swift
//  HugeImageViewer
//
//  Created by Taras Pasichnyk on 24.07.2020.
//  Copyright © 2020 Taras Pasichnyk. All rights reserved.
//

import Foundation

protocol ImageListPresenterProtocol {
    func loadImages()
    func didSelectItem(_ imageItem: ImageItem)
}

class ImageListPresenter {

    // MARK: - Variables

    weak var coordinator: ImageFlowCoordinatorProtocol?
    unowned let view: ImageListViewProtocol
    var imageLoader: ImageLoader

    // MARK: - Lifecycle

    init(coordinator: ImageFlowCoordinatorProtocol?,
         view: ImageListViewProtocol,
        imageLoader: ImageLoader) {
        self.coordinator = coordinator
        self.view = view
        self.imageLoader = imageLoader
        self.imageLoader.delegate = self
    }
}

// MARK: - ImageListPresenterProtocol

extension ImageListPresenter: ImageListPresenterProtocol {
    
    func loadImages() {
        imageLoader.loadAllImages()
    }
    
    func didSelectItem(_ imageItem: ImageItem) {
        if imageItem.fullImage == nil {
            view.displayImageRemovedError()
            return
        }
        coordinator?.displayFullImage(imageItem)
    }
}

// MARK: - ImageLoaderDelegate

extension ImageListPresenter: ImageLoaderDelegate {
    
    func imageLoader(_ imageLoader: ImageLoader, didFInishCreatingThumbnailsForImageItems imageItems: [ImageItem]) {
        view.setImageItems(imageItems)
    }
}
