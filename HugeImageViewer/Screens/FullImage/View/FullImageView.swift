//
//  FullImageView.swift
//  HugeImageViewer
//
//  Created by Taras Pasichnyk on 28.07.2020.
//  Copyright © 2020 Taras Pasichnyk. All rights reserved.
//

import UIKit

protocol FullImageViewProtocol: class {
    func setImage(_ image: UIImage?)
}

class FullImageView: UIViewController {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var closeButton: UIButton!
    
    // MARK: - Variables
    
    private var imageScrollView: ImageScrollView!

    var presenter: FullImagePresenterProtocol!
    
    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureAppearance()
        configureScrollView()
        
        presenter.loadImage()
    }
    
    // MARK: - Configuration
    
    private func configureAppearance() {
        view.backgroundColor = .black
        
        closeButton.tintColor = .white
    }
    
    private func configureScrollView() {
        imageScrollView = ImageScrollView(frame: view.bounds)
        view.insertSubview(imageScrollView, at: 0)
        imageScrollView.translatesAutoresizingMaskIntoConstraints = false
        imageScrollView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        imageScrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        imageScrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        imageScrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
    }
    
    // MARK: - IBActions
    
    @IBAction func closeButtonTouched(_ sender: UIButton) {
        presenter.didTouchCloseButton()
    }
}

// MARK: - FullImageViewProtocol

extension FullImageView: FullImageViewProtocol {
    
    func setImage(_ image: UIImage?) {
        guard let image = image else {
            print("Image is nil")
            return
        }
        imageScrollView.set(image: image)
    }
}
