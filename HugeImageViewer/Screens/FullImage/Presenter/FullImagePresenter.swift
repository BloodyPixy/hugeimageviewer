//
//  FullImagePresenter.swift
//  HugeImageViewer
//
//  Created by Taras Pasichnyk on 28.07.2020.
//  Copyright © 2020 Taras Pasichnyk. All rights reserved.
//

import Foundation

protocol FullImagePresenterProtocol {
    func loadImage()
    func didTouchCloseButton()
}

class FullImagePresenter {

    // MARK: - Variables

    weak var coordinator: ImageFlowCoordinatorProtocol?
    unowned let view: FullImageViewProtocol
    var imageItem: ImageItem

    // MARK: - Lifecycle
    
    init(coordinator: ImageFlowCoordinatorProtocol?,
         view: FullImageViewProtocol,
         imageItem: ImageItem) {
        self.coordinator = coordinator
        self.view = view
        self.imageItem = imageItem
    }
}

// MARK: - ImageListPresenterProtocol

extension FullImagePresenter: FullImagePresenterProtocol {
    
    func loadImage() {
        view.setImage(imageItem.fullImage)
    }
    
        
    func didTouchCloseButton() {
        coordinator?.closeFullImage()
    }
}
