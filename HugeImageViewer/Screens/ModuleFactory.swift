//
//  ModuleFactory.swift
//  HugeImageViewer
//
//  Created by Taras Pasichnyk on 24.07.2020.
//  Copyright © 2020 Taras Pasichnyk. All rights reserved.
//

import Foundation

struct ModuleFactory {
    static func makeImageListView(
        coordinator: ImageFlowCoordinatorProtocol?,
        imageLoader: ImageLoader) -> ImageListView {
        let imageListView = ImageListView.instantiateVC()
        let presenter =
            ImageListPresenter(
                coordinator: coordinator,
                view: imageListView,
                imageLoader: imageLoader)
        imageListView.presenter = presenter
        return imageListView
    }
    
    static func makeFullImageView(
        coordinator: ImageFlowCoordinatorProtocol?,
        imageItem: ImageItem) -> FullImageView {
        let fullImageView = FullImageView.instantiateVC()
        let presenter =
            FullImagePresenter(
                coordinator: coordinator,
                view: fullImageView,
                imageItem: imageItem)
        fullImageView.presenter = presenter
        return fullImageView
    }
}
