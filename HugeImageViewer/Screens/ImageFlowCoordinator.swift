//
//  ImageFlowCoordinator.swift
//  HugeImageViewer
//
//  Created by Taras Pasichnyk on 24.07.2020.
//  Copyright © 2020 Taras Pasichnyk. All rights reserved.
//

import UIKit

protocol ImageFlowCoordinatorProtocol: class {
    func displayImageList()
    func displayFullImage(_ imageItem: ImageItem)
    func closeFullImage()
}

class ImageFlowCoordinator: Coordinator {

    // MARK: - Properties

    private let navigationController: UINavigationController

    // MARK: - Lifecycle

    init(navigationController: UINavigationController = UINavigationController()) {
        self.navigationController = navigationController
    }

    // MARK: Coordinator

    func start() {
        displayImageList()
    }
}

// MARK: - ImageFlowCoordinatorProtocol

extension ImageFlowCoordinator: ImageFlowCoordinatorProtocol {
    func displayImageList() {
        let imageLoader =
            DocumentsImageLoader(
                fileManager: .default)
        let imageListView =
            ModuleFactory.makeImageListView(
                coordinator: self,
                imageLoader: imageLoader)
        navigationController.viewControllers = [imageListView]
    }

    func displayFullImage(_ imageItem: ImageItem) {
        let fullImageView =
        ModuleFactory.makeFullImageView(
            coordinator: self,
            imageItem: imageItem)
        navigationController.topViewController?.present(fullImageView, animated: true)
    }

    func closeFullImage() {
        navigationController.topViewController?.presentedViewController?.dismiss(animated: true)
    }
}
