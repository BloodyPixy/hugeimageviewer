//
//  UICollectionView+Cell.swift
//  HugeImageViewer
//
//  Created by Taras Pasichnyk on 27.07.2020.
//  Copyright © 2020 Taras Pasichnyk. All rights reserved.
//

import UIKit

/// Generic methods to wrap registering and dequeing cells for UICollectionView
extension UICollectionView {
    func dequeueCell<T: UICollectionViewCell>(_ type: T.Type, indexPath: IndexPath) -> T {
        return self.dequeueReusableCell(
            withReuseIdentifier: type.identifier,
            for: indexPath) as! T
    }

    func registerCell<T: UICollectionViewCell>( _ type: T.Type) {
        self.register(type.nib, forCellWithReuseIdentifier: type.identifier)
    }
}

