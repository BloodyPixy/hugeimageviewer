//
//  UIViewController+Instantiate.swift
//  HugeImageViewer
//
//  Created by Taras Pasichnyk on 24.07.2020.
//  Copyright © 2020 Taras Pasichnyk. All rights reserved.
//

import UIKit

extension UIViewController {

    /// Creates UIViewController from storyboars with a same name

    static func instantiateVC() -> Self {
        let storyboard = UIStoryboard(name: Self.identifier, bundle: nil)
        return storyboard.instantiateViewController(identifier: Self.identifier) as! Self
    }
}
