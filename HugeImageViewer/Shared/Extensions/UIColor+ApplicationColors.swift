//
//  UIColor+ApplicationColors.swift
//  HugeImageViewer
//
//  Created by Taras Pasichnyk on 28.07.2020.
//  Copyright © 2020 Taras Pasichnyk. All rights reserved.
//

import UIKit

// Extension that defines special colors used in the application
extension UIColor {

    static var appGrey: UIColor {
        return UIColor(red: 33 / 254, green: 33 / 254, blue: 33 / 254, alpha: 1.0)
    }

    static var appAlmostTransparent: UIColor {
        return UIColor(red: 1 / 254, green: 1 / 254, blue: 1 / 254, alpha: 0.7)
    }

    static var appLightGrey: UIColor {
        return UIColor(red: 64 / 254, green: 64 / 254, blue: 64 / 254, alpha: 1.0)
    }

    static var appYellow: UIColor {
        return UIColor(red: 252 / 254, green: 208 / 254, blue: 82 / 254, alpha: 1.0)
    }
}
