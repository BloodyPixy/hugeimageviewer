//
//  ImageItemCell.swift
//  HugeImageViewer
//
//  Created by Taras Pasichnyk on 27.07.2020.
//  Copyright © 2020 Taras Pasichnyk. All rights reserved.
//

import UIKit

class ImageItemCell: UICollectionViewCell {
    
    // MARK: - IBOutlets

    @IBOutlet weak var imageView: UIImageView!
    
    // MARK: - Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        configureSubviews()
    }
    
    // MARK: - Configuration
    
    private func configureSubviews() {
        imageView.contentMode = .scaleAspectFill
    }
    
    // MARK: - Methods
    
    func setImage(_ image: UIImage?) {
        imageView.image = image
    }
}
