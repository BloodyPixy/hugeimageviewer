//
//  ImageListEmptyCell.swift
//  HugeImageViewer
//
//  Created by Taras Pasichnyk on 28.07.2020.
//  Copyright © 2020 Taras Pasichnyk. All rights reserved.
//

import UIKit

protocol ImageListEmptyCellDelegate: class {
    func imageListEmptyCellDidTouchActionButton(_ cell: ImageListEmptyCell)
}

class ImageListEmptyCell: UICollectionViewCell {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var mainStackView: UIStackView!
    @IBOutlet weak var staticStackView: UIStackView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var activeStackView: UIStackView!
    @IBOutlet weak var actionButton: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    // MARK: - Variables
    
    weak var delegate: ImageListEmptyCellDelegate?
    
    // MARK: - Lifecycle

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureSubviews()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        activityIndicator.stopAnimating()
        actionButton.isHidden = false
        activityIndicator.isHidden = true
    }
    
    // MARK: - Configuration
    
    private func configureSubviews() {
        containerView.backgroundColor = .appGrey
        
        titleLabel.font = UIFont.boldSystemFont(ofSize: 16.0)
        titleLabel.textColor = .white
        
        descriptionLabel.font = UIFont.systemFont(ofSize: 14.0)
        descriptionLabel.textColor = .white
        
        actionButton.setTitleColor(.appGrey, for: .normal)
        actionButton.backgroundColor = .white
        actionButton.layer.cornerRadius = 8.0
        actionButton.layer.masksToBounds = false
        
        activityIndicator.color = .white
        activityIndicator.isHidden = true
    }
    
    // MARK: - Actions

    @IBAction func actionButtonTouched(_ sender: UIButton) {
        actionButton.isHidden = true
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
        // Delay added only to illustrate activity that will happen when there are more items
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            self.delegate?.imageListEmptyCellDidTouchActionButton(self)
        }
    }
    
    // MARK: - Methods
    
    func setTitle(_ titleText: String?, descriptionText: String?) {
        titleLabel.text = titleText
        descriptionLabel.text = descriptionText
    }
    
    func setActionButtonTitleText(_ text: String?) {
        actionButton.setTitle(text, for: .normal)
    }
}
