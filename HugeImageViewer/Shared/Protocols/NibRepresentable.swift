//
//  NibRepresentable.swift
//  HugeImageViewer
//
//  Created by Taras Pasichnyk on 24.07.2020.
//  Copyright © 2020 Taras Pasichnyk. All rights reserved.
//

import UIKit

/// Used to get a unique identifier and a nib from conforming object

protocol NibRepresentable {
    static var nib: UINib { get }
    static var identifier: String { get }
}

extension NibRepresentable {
    static var identifier: String {
        return String(describing: Self.self)
    }
}

extension NibRepresentable where Self: UIView {
    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
}

extension NibRepresentable where Self: UIViewController {
    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
}

extension UIView: NibRepresentable {}
extension UIViewController: NibRepresentable {}

