//
//  Coordinator.swift
//  HugeImageViewer
//
//  Created by Taras Pasichnyk on 24.07.2020.
//  Copyright © 2020 Taras Pasichnyk. All rights reserved.
//

import Foundation

/// Simplest possible Coordinator protocol

protocol Coordinator {
    func start()
}
