//
//  ApplicationCoordinator.swift
//  HugeImageViewer
//
//  Created by Taras Pasichnyk on 24.07.2020.
//  Copyright © 2020 Taras Pasichnyk. All rights reserved.
//

import UIKit

final class ApplicationCoordinator: Coordinator {
    
    // MARK: - Properties

    private let window: UIWindow
    private let navigationController: UINavigationController
    var starterCoordinator: Coordinator?

    // MARK: - Lifecycle
    
    init(window: UIWindow = UIWindow(),
         navigationController: UINavigationController = UINavigationController()) {
        self.window = window
        self.navigationController = navigationController
        setupWindow()
        setupStarterCoordinator()
    }
    
    // MARK: - Methods

    func setupWindow() {
        self.window.rootViewController = navigationController
        self.window.makeKeyAndVisible()
    }

    func setupStarterCoordinator() {
        starterCoordinator = ImageFlowCoordinator(navigationController: navigationController)
    }

    func start() {
        starterCoordinator?.start()
    }
}
