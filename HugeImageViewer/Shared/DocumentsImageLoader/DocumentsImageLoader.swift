//
//  DocumentsImageLoader.swift
//  HugeImageViewer
//
//  Created by Taras Pasichnyk on 24.07.2020.
//  Copyright © 2020 Taras Pasichnyk. All rights reserved.
//

import Foundation
import QuickLook

protocol ImageLoaderDelegate: class {
    func imageLoader(_ imageLoader: ImageLoader, didFInishCreatingThumbnailsForImageItems imageItems: [ImageItem])
}

final class DocumentsImageLoader: ImageLoader {
    
    // MARK: - Properties
    
    private let fileManager: FileManager
    private let thumbnailGenerator: ThumbnailGenerator
    
    // MARK: - Variables
    
    weak var delegate: ImageLoaderDelegate?
    var imageItems: [ImageItem] = []
    
    private func filenameHasTiffExtension(_ filename: String) -> Bool {
        return filename.hasSuffix(".tiff")
            || filename.hasSuffix(".tif")
            || filename.hasSuffix(".TIFF")
            || filename.hasSuffix(".TIF")
    }
    
    // MARK: - Lifecycle
    
    init(
        fileManager: FileManager = .default) {
        self.fileManager = fileManager
        self.thumbnailGenerator = ThumbnailGenerator()
        self.thumbnailGenerator.delegate = self
    }
    
    // MARK: - ImageLoader
    
    func loadAllImages() {
        imageItems = []
        let documentsUrl = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
        
        let documentsDirectory = NSHomeDirectory().appending("/Documents")
        let directoryEnumerator = fileManager.enumerator(atPath: documentsDirectory)

        while let filename = directoryEnumerator?.nextObject() as? String {
            if filenameHasTiffExtension(filename) {
                let url = documentsUrl.appendingPathComponent(filename)
                let thumbnailOutputUrl = url.appendingPathExtension("_thumbnail")
            
                if !fileManager.fileExists(atPath: thumbnailOutputUrl.path) {
                    thumbnailGenerator.append(inputUrl: url, outputUrl: thumbnailOutputUrl)
                }
                
                let item =
                    ImageItem(
                        name: filename,
                        thumbnailPath: thumbnailOutputUrl,
                        imagePath: url)
                imageItems.append(item)
            }
        }
        thumbnailGenerator.execute()
    }
}

// MARK: - ThumbnailGeneratorDelegate

extension DocumentsImageLoader: ThumbnailGeneratorDelegate {
    func thumbnailGeneratorDidFinishBatch(_ generator: ThumbnailGenerator) {
        delegate?.imageLoader(self, didFInishCreatingThumbnailsForImageItems: imageItems)
    }
}
