//
//  ImageLoader.swift
//  HugeImageViewer
//
//  Created by Taras Pasichnyk on 24.07.2020.
//  Copyright © 2020 Taras Pasichnyk. All rights reserved.
//

import Foundation

protocol ImageLoader {
    var delegate: ImageLoaderDelegate? { get set }
    func loadAllImages()
}
