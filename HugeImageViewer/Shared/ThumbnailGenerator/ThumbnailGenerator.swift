//
//  ThumbnailGenerator.swift
//  HugeImageViewer
//
//  Created by Taras Pasichnyk on 28.07.2020.
//  Copyright © 2020 Taras Pasichnyk. All rights reserved.
//

import Foundation
import QuickLook

protocol ThumbnailGeneratorDelegate: class {
    func thumbnailGeneratorDidFinishBatch(_ generator: ThumbnailGenerator)
}

final class ThumbnailGenerator {
    
    // MARK: - Constants
    
    let size: CGSize = CGSize(width: 64.0, height: 64.0)
    let scale: CGFloat = UIScreen.main.scale
    
    // MARK: - Variables
    
    weak var delegate: ThumbnailGeneratorDelegate?
    
    var inputOtputUrls: [URL: URL] = [:]

    // MARK: - Methods
    
    func append(inputUrl: URL, outputUrl: URL) {
        self.inputOtputUrls[inputUrl] = outputUrl
    }
    
    func execute() {
        if inputOtputUrls.isEmpty {
            delegate?.thumbnailGeneratorDidFinishBatch(self)
            return
        }
        DispatchQueue.global(qos: .userInteractive).async {
            let results = self.inputOtputUrls.map { (inputUrl, outputUrl) in
                self.createThumbnailFor(inputUrl, at: outputUrl)
            }
            print(results)
            DispatchQueue.main.async {
                self.delegate?.thumbnailGeneratorDidFinishBatch(self)
            }
        }
    }
    
    // MARK: - Private
    
    /// Uses quick look to asynchronously create best possible thumbnail image at path
    private func createThumbnailFor(_ inputUrl: URL, at outputUrl: URL) -> Result<Bool, Error> {
        
        var result: Result<Bool, Error>!
        
        let semaphore = DispatchSemaphore(value: 0)
        
        // Create the thumbnail request.
        let request =
            QLThumbnailGenerator.Request(
                fileAt: inputUrl,
                size: size,
                scale: scale,
                representationTypes: .all)
        
        // Retrieve the singleton instance of the thumbnail generator and generate the thumbnails.
        let generator = QLThumbnailGenerator.shared
        // Problem with a content type. Asked question on SO, as could not find an answer - https://stackoverflow.com/questions/63131945/qlthumbnailgenerator-savebestrepresentation-wrong-content-type-swift-ios-13
//        generator.saveBestRepresentation(for: request, to: outputUrl, contentType: "image/jpg") { (error) in
//            if let error = error {
//                print(error.localizedDescription)
//            }
//        }
        // As a temporary solution, creating preview with native functionality and saving it manually
        generator.generateBestRepresentation(for: request) { (thumbnail, error) in
            if let thumbnail = thumbnail {
                if let data = thumbnail.uiImage.jpegData(compressionQuality: 1.0) {
                    do {
                        try data.write(to: outputUrl)
                        result = .success(true)
                    } catch let error {
                        print(error.localizedDescription)
                        result = .failure(error)
                    }
                }
            }
            if let error = error {
                print(error.localizedDescription)
                result = .failure(error)
            }
            semaphore.signal()
        }
        
        _ = semaphore.wait(wallTimeout: .distantFuture)
        
        return result
    }
}
