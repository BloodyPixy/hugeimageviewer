//
//  ImageItem.swift
//  HugeImageViewer
//
//  Created by Taras Pasichnyk on 27.07.2020.
//  Copyright © 2020 Taras Pasichnyk. All rights reserved.
//

import UIKit

struct ImageItem {
    let name: String
    let thumbnailPath: URL?
    let imagePath: URL
    
    var thumbnailImage: UIImage? {
        guard let thumbnailPath = thumbnailPath else {
            print("Thumbnail path is nil for image '\(name)'")
            return nil
        }
        if let data = try? Data(contentsOf: thumbnailPath) {
            return UIImage(data: data)
        }
        return nil
    }
    
    var fullImage: UIImage? {
        if let data = try? Data(contentsOf: imagePath) {
            return UIImage(data: data)
        }
        return nil
    }
}
