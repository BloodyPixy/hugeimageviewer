# HUGE Image Viewer

### **Task**

Design an iOS application that handles very large images
Your task is to write an iOS application, which is capable of handling very large uncompressed images (e.g. 50 MB TIFF images).
Requirements
The images are expected to be loaded dynamically from the app bundle `Documents` folder, and the user should be able to add more using the iTunes file sharing system (see `UIFileSharingEnabled` for more details).
The images should be rendered correctly inside a `UITableView` or a `UICollectionView` object.
Use of 3rd party libraries is prohibited.
Your code should be structured and packaged with highest maintainability in mind.

### **Architecture**

After reviewing requirements and the designs I decided to implement MVP+C architecture in this project as it provides good separation of responsibilities between modules and testability. 
- `ApplicationCoordinator` - provides application entry point
- `ModuleFactory` - provides configured MVP modules
- `UITableView` and `UICollectionView` extension for registering and dequeing cells

`SwiftLint` - added it only for a sake of code cleanliness. Library is not needed. If it is not installed in the system, run script will be ignored.

### **Screens**

- `ImageListView` - displays thumbnails of *.tiff images that are located in `Documents` folder inside a collection view. Has empty state handling and gives user ability to rescan a documents folder. If user tries to open image, that does not longer exist inside specified folder, popup is presented and rescan option proposed. 
- `FullImageView` - displays zoomable and movable full image inside a custom `UIScrollView`.

### **Main Modules**

- `ImageLoader` - uses native `FIleManager` and `DirectoryEnumerator` to scan and retrive image data from a folder.
- `ThumbnailGenerator` - uses native `QuickLook` API to generate thumbnail previews for big images. Sometime does not work properly for some images. Instead of a thumbnail a simple system icon may be presented. When looking on full image everything is fine.

### **Important**
Application will only see *.tiff files if they have `.tiff`, `.tif`, `.TIF` or `.TIFF` extensions.

### **Conclusion**
There is always a place for imrovement. It took me 8 - 12 hours to complete this project. Please, take a look at the whole codebase to understand how it works and feel free to contact me anytime. Full git history is available.
